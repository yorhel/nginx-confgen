macro bind_tls {
    listen 0.0.0.0:80 tls;
    description $name; # Global $name
    not_description $something; # Not interpolated
}


# Macro <name> <arg1> <arg2> .. <contents>
# Arguments can be a:
#   $var  -> scalar argument
#   @var  -> array argument,
#            represents zero or more arguments,
#            can only be passed to other directives,
#            if present, must be last argument or before &var
#   &var  -> block argument, must be the last argument, if present
macro server_https $name @alias &block {
    # $name here shadows the global $name

    pre_set $something 2;

    # Anything can go inside the macro contents
    server {
        # Macro invocation inside a macro; $name, @alias, &block and $something
        # are not available to the macro.
        bind_tls;

        server_name $name @alias;
        cert_something "/etc/nginx/certs/$name.crt";

        # expands to the block argument, same rules as a macro invocation:
        &block;
    }
}
